package hr.ferit.ivangudelj.orwma.lv2.janadukic_lv3_prrimjer2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.transition.Slide;
import android.widget.TableLayout;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setUpPager();
    }

    private void setUpPager() {
        //instancu tog adaptera
        List<Fragment> fragmentList = new ArrayList();
        fragmentList.add(SlideFragment.newInstance("This is fragment #1"));
        fragmentList.add(SlideFragment.newInstance("This is fragment #2"));
        fragmentList.add(SlideFragment.newInstance("This is fragment #3"));
        fragmentList.add(SlideFragment.newInstance("This is fragment #4"));
        //fragmentList.add(new MessageFragment()); ali ga moram prvo ubacit iz proslog primjera

        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), fragmentList);
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void initViews() {
        mViewPager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tabLayoout);
    }
}