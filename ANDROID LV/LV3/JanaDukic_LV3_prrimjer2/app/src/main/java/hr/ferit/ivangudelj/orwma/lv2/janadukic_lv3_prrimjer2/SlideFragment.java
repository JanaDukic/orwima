package hr.ferit.ivangudelj.orwma.lv2.janadukic_lv3_prrimjer2;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class SlideFragment extends Fragment {
    private static final String BUNDLE_KEY = "display message";
    private TextView mMessageTv;

    public static  SlideFragment newInstance(String message){
        //static znaci da mozemo pristupit i bez inicijalizacije objektaž
        SlideFragment fragment = new SlideFragment();
        Bundle args = new Bundle(); //spremnik podataka za ubacivanje varijabli za neki key, od int do boolean... sve
        args.putString(BUNDLE_KEY,message);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMessageTv = view.findViewById(R.id.tvMessage);
        setUpTextView();
    }

    private void setUpTextView() {
        String message = getString(R.string.hello_world);
        if(getArguments() != null){
            String argMessage = getArguments().getString(BUNDLE_KEY, message);
            if(argMessage != null && !argMessage.isEmpty()){
                message = argMessage;
            }
        }
        mMessageTv.setText(message);
    }


}