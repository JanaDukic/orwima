package hr.ferit.ivangudelj.orwma.lv2.janadukic_lv3_prrimjer2;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mItems;
    private static final int NUM_PAGES = 3; //koliko imamo stranica

    public PagerAdapter(@NonNull FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        mItems = fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        /*
        switch (position){
            case 0:
                return SlideFragment.newInstance("This is fragment #1");
            case 1:
                return SlideFragment.newInstance("This is fragment #2");
            default:
                return SlideFragment.newInstance("This is fragment #3");
        }
         */
        return mItems.get(position);
    }

    @Override
    public int getCount() {
        //return NUM_PAGES; //vraca koliko fragmenata ce prikazati
        return mItems.size();
    }

    public CharSequence getPageTitle(int position){
        return "Tab #" + (position + 1);
    }
}
