package hr.ferit.ivangudelj.orwma.lv2.janadukic_lv3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements ButtonClickListener {

    private  MessageFragment mMessageFragment;
    private  InputFragment mInputFragment;

    private FragmentManager mFragmentManager;

    private boolean mSwitched = false;  // slovo m oznacava da je ta varijabla promjenjiva


    //metoda koja se prva poziva
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();
        setUpViews();
        setUpFragments();
    }

    private void setUpFragments() {
        //iniciajlizacija fragmenta
        mMessageFragment = new MessageFragment();
        mInputFragment = new InputFragment();
        //sad ih moramo ubaciti u kontejnere koje smo prije dodali

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.firstFragment, mMessageFragment);
        fragmentTransaction.add(R.id.secondFragment, mInputFragment);
        fragmentTransaction.commit();
    }

    private void setUpViews() {
        findViewById(R.id.btSwitch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragments();
            }
        });
    }

    //paziti da ne bude anonimka klasa kod kreiranja(prvo smo zapisali switchFraments zatim smo kreirali tu dolje sa klikom
    private void switchFragments() {
        removeFragments();
        mSwitched = !mSwitched; //svakim pritiskom mu se promjeni stanje
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
       if(mSwitched){
           fragmentTransaction.replace(R.id.firstFragment, mInputFragment);
           fragmentTransaction.replace(R.id.secondFragment,mMessageFragment);
       }else {
           fragmentTransaction.replace(R.id.firstFragment, mMessageFragment);
           fragmentTransaction.replace(R.id.secondFragment,mInputFragment);
       }
        fragmentTransaction.commit();
    }

    private void removeFragments() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.remove(mMessageFragment);
        fragmentTransaction.remove(mInputFragment);
        fragmentTransaction.commit();
        mFragmentManager.executePendingTransactions(); //inace se srusi jer se ova ne zavrsi prije poziva nove transakcije
        //prisiljava commit da se odradi odma
    }


    @Override
    public void onButtonClicked(String message) {
        mMessageFragment.displayMessage(message);
    }
}