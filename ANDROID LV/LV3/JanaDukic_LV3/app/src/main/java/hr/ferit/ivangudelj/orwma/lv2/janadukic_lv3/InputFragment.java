package hr.ferit.ivangudelj.orwma.lv2.janadukic_lv3;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class InputFragment extends Fragment {

    private EditText mEditText;
    private Button mSubmitButton;

    private ButtonClickListener mListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditText = view.findViewById(R.id.edMessage);
        mSubmitButton = view.findViewById(R.id.btSubmit);
        setupListeners();
    }

    private void setupListeners() {
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onButtonClicked(mEditText.getText().toString()); //nismo koristili textWatcher jer smo imali  jednostavniji primjer
                }
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //jeli kontekst instanca listenera kkoje smo kreirali i onda je inicijaliziramo
        //nesto ce biti instanca btn click listenera on se mora implementirati (fali u main activitiju implements btnClickListener)
        if(context instanceof ButtonClickListener){
            mListener = (ButtonClickListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null; //kad se otkaci da ne slusa vise promjene
    }
}