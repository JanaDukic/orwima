package com.example.janadukic_viewpagerapp_lv3;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.ListFragment;

import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter {


    private List<Fragment> mItems;
    private static final int NUM_PAGES = 4;

    public PagerAdapter(@NonNull FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        mItems = fragmentList;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    public CharSequence getPageTitle(int position){
        return "#" + (position+1);
    }
}
