package com.example.janadukic_viewpagerapp_lv3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.graphics.pdf.PdfDocument;
import android.icu.util.Measure;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ToxicBakery.viewpager.transforms.CubeInTransformer;
import com.ToxicBakery.viewpager.transforms.CubeOutTransformer;
import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ButtonClickListener{

   private ViewPager mViewPager;
   private TabLayout mTabLayout;
    private  MessageFragment mMessageFragment;
    private  InputFragment mInputFragment;
    private FragmentManager mFragmentManager;
    private  TextPictureFragment mTextFragment;
    private RecyclerAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();
        initViews();
        setUpPager();

    }

    private void setUpPager() {
        List<Fragment> fragmentList = new ArrayList();
        mMessageFragment = new MessageFragment();
        mInputFragment = InputFragment.NewInstance("input");
        mTextFragment = TextPictureFragment.newInstance(R.layout.fragment_hello_world);

        fragmentList.add(mInputFragment);
        fragmentList.add(mMessageFragment);
        fragmentList.add(mTextFragment);
        fragmentList.add(TextPictureFragment.newInstance(R.layout.picturefragment));

        PagerAdapter adapter= new PagerAdapter(getSupportFragmentManager(), fragmentList);
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setPageTransformer(true, new CubeOutTransformer());
        mViewPager.setOffscreenPageLimit(4); //dodano jer je svaki puta kreirao novi fragment na povratak na main fragment a ovako je ogranicen

    }

    private void initViews() {
        mViewPager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tabLayoout);

    }


    @Override
    public void onButtonClicked(String message) {
        EditText editText = findViewById(R.id.edMessage);
        if(editText.getText().toString().equals("")){
            Toast.makeText(this,getString(R.string.Toast_warning), Toast.LENGTH_LONG).show();
        }else{
            mMessageFragment.displayMessage(message);
            mViewPager.setCurrentItem(1,true);
            mTextFragment.SendToAdapter(message);
            editText.setText("");
        }
    }




}