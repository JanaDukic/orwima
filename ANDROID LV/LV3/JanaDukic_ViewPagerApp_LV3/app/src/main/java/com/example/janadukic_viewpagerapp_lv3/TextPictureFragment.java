package com.example.janadukic_viewpagerapp_lv3;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class TextPictureFragment extends Fragment implements ImageClickListener{
    private int mLayoutNumber;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private ImageButton imageButton;

    public TextPictureFragment(int layout){
        mLayoutNumber = layout;
    }

    public static TextPictureFragment newInstance(int layout){
        return new TextPictureFragment(layout);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(mLayoutNumber, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mLayoutNumber == R.layout.fragment_hello_world) {
            setupRecycler(view);
            setupRecyclerData();
        }
    }


    private void setupRecycler(View view){
        mRecyclerView = view.findViewById(R.id.recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        mAdapter = new RecyclerAdapter(this); //tu neznam sta ide u zagradu
        mRecyclerView.setAdapter(mAdapter);
        this.imageButton = (ImageButton) view.findViewById(R.id.tvX);
    }

    private void setupRecyclerData() {
        List<String> data = new ArrayList<>();
        data.add("JAna");
        mAdapter.addData(data);
    }

    @Override
    public void onImageClick(int position) {
        mAdapter.removeCell(position);
    }

    public void SendToAdapter(String message) {
        mAdapter.addNewCell(message);
    }
}