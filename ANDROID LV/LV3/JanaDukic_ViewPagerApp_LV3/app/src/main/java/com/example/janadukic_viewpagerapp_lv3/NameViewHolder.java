package com.example.janadukic_viewpagerapp_lv3;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private TextView mTvName;
    public ImageButton mImageButton;
    private ImageClickListener mClickListener;
    private int mPosition;

    public NameViewHolder(@NonNull View itemView, ImageClickListener listener) {
        super(itemView);
        mClickListener = listener;
        mTvName = itemView.findViewById(R.id.tvName);
        mImageButton = itemView.findViewById(R.id.tvX);
        mImageButton.setOnClickListener(this);
    }
    public TextView getView(){return mTvName;}

    public void setTvName(String name) {
        mTvName.setText(name);
    }

    @Override
    public void onClick(View v) {
        mPosition = getAdapterPosition();
        mClickListener.onImageClick(mPosition);
    }
}
