package com.example.janadukic_viewpagerapp_lv3;

public interface ButtonClickListener {
    void onButtonClicked(String message);
}
