package com.example.janadukic_viewpagerapp_lv3;

public interface ImageClickListener {
    void onImageClick(int position);
}
