package hr.ferit.lv2_radnasatu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NameClickListener {

    private RecyclerView recyclerView;
    private List<String> listaData;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setupData();
        setupRecycleView();


    }

    @Override
    public void onNameClick(int position) {
        Log.d("onNameClick", "Element " + position);
    }

    private void setupRecycleView(){
        recyclerView = findViewById(R.id.recycleview22);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        CustomAdapter customAdapter = new CustomAdapter(listaData, this);
        recyclerView.setAdapter(customAdapter);

    }

    private void setupData(){
        listaData = new ArrayList<>();
        listaData.add("prvi");
        listaData.add("drugi");
        listaData.add("treci");
        listaData.add("cetvrti");
        listaData.add("peti");
        listaData.add("sesti");
        listaData.add("sedmi");
        listaData.add("osmi");
        listaData.add("deveti");
        listaData.add("deseti");
        listaData.add("jedanaesti");
        listaData.add("dvanaesti");
        listaData.add("trinaesti");
        listaData.add("cetrnaesti");
        listaData.add("petnaesti");
        listaData.add("sesnaesti");
        listaData.add("sedamnaesti");


    }
}