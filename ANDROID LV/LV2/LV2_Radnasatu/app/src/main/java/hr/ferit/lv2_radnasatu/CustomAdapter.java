package hr.ferit.lv2_radnasatu;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.NameViewHolder>{

    private List<String> listData;
    private NameClickListener listener;

    public CustomAdapter(List<String> listData,NameClickListener listener) {
        this.listData = listData;
        this.listener = listener;
    }

    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View listItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem, parent, false);
        return new NameViewHolder(listItemView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull NameViewHolder holder, int position) {
        Log.d("onBindViewHolder", "Element"+position);
        holder.setName(listData.get(position));

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static class NameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final TextView nametextView;
        private NameClickListener listener;


        public NameViewHolder(@NonNull View itemView, NameClickListener listener) {
            super(itemView);
            nametextView = itemView.findViewById(R.id.nameTestView);
            this.listener = listener;

            itemView.setOnClickListener(this);

        }

        public void setName(String name){
            nametextView.setText(name);
        }


        @Override
        public void onClick(View v) {
            listener.onNameClick(getAdapterPosition());
        }
    }
}
