package com.example.lv2_dz_evidencijaprisutnostistudenata_janadukic;

public interface ImageClickListener {
    void onImageClick(int position);
}
