package com.example.lv2_dz_evidencijaprisutnostistudenata_janadukic;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<NameViewHolder> {

    private List<String> dataList = new ArrayList<>();
    private ImageClickListener clickListener;

    public RecyclerAdapter(ImageClickListener listener) {
        this.clickListener = listener;
    }

    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cellVieW = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem,parent,false);
        return new NameViewHolder(cellVieW, clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NameViewHolder nameViewHolder, int position) {
        nameViewHolder.setTvName(dataList.get(position));
    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addData(List<String> data){
        this.dataList.clear();
        this.dataList.addAll(data);
        notifyDataSetChanged();
    }

    public void addNewCell(String name){
        int position = getItemCount();
        if(dataList.size() >= position){
            dataList.add(position,name);
            notifyItemInserted(position);
        }
    }

    public void removeCell(int position){
        if(dataList.size() > position){
            dataList.remove(position);
            notifyItemRemoved(position);
        }
    }



}
