package com.example.lv2_dz_evidencijaprisutnostistudenata_janadukic;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView tvName;
    private ImageClickListener clickListener;
    public ImageButton imageButton;
    private int position;



    public NameViewHolder(@NonNull View itemView, ImageClickListener listener){
        super(itemView);
        this.clickListener = listener;
        tvName = itemView.findViewById(R.id.tvName);
        //itemView.setOnClickListener(this);
        this.imageButton = itemView.findViewById(R.id.tvX);
        this.imageButton.setOnClickListener(this);
    }

    public void setTvName(String name) {
       tvName.setText(name);
    }

    @Override
    public void onClick(View view) {
        position = getAdapterPosition();
        clickListener.onImageClick(position);
    }



}
