package com.example.lv2_dz_evidencijaprisutnostistudenata_janadukic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.strictmode.WebViewMethodCalledOnWrongThreadViolation;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ImageClickListener {

    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private EditText editText;
    private ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupRecycler();
        setupRecyclerData();
    }


    private void setupRecycler() {
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerAdapter(this);
        recyclerView.setAdapter(adapter);
        this.imageButton = (ImageButton) findViewById(R.id.tvX);

    }

    private void setupRecyclerData() {
        List<String> data = new ArrayList<>();
        data.add("Ana");
        data.add("Marko");
        data.add("Pero");
        adapter.addData(data);
    }

    public void addCell(View view) {
        this.editText = (EditText) findViewById(R.id.etIme);
        if(editText.getText().toString().equals("")){
            Toast.makeText(this,"You need to enter name", Toast.LENGTH_LONG).show();
        }else{
            adapter.addNewCell(editText.getText().toString());
            this.editText.setText("");
        }
    }


    @Override
    public void onImageClick(int position) {
       // Toast.makeText(this, "position: " + position, Toast.LENGTH_SHORT).show();
        adapter.removeCell(position);
    }



}